from django.contrib.auth.models import AbstractUser
from django.db import models

class CustomUser(AbstractUser):
    pass

class Resident(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, null=True, blank=True)
    name = models.CharField(max_length=100)
    surname = models.CharField(max_length=100)
    address = models.CharField(max_length=255)
    apartment_number = models.IntegerField()

    def __str__(self):
        return f"{self.name} {self.surname}"


class BillType(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Bill(models.Model):
    resident = models.ForeignKey("Resident", on_delete=models.CASCADE)
    bill_type = models.ForeignKey("BillType", on_delete=models.CASCADE)
    amount = models.FloatField()
    issued_date = models.DateField()
    payment_date = models.DateField(blank=True, null=True)
    is_payed = models.BooleanField(blank=True, null=True)
    payment_status = models.CharField(max_length=10)
    account = models.ForeignKey("Account", on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return f"Счет для {self.resident.name} {self.resident.surname}"


class Account(models.Model):
    resident = models.ForeignKey("Resident", on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    balance = models.FloatField()

    def __str__(self):
        return self.name
