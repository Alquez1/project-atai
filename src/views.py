from django.contrib import messages
from django.views.decorators.http import require_POST
from rest_framework import viewsets
from .models import Resident, Bill, BillType, Account
from .serializers import ResidentSerializer, BillSerializer, BillTypeSerializer, AccountSerializer
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from .forms import RegisterForm, LoginForm
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, redirect
from django.utils import timezone


class ResidentViewSet(viewsets.ModelViewSet):
    queryset = Resident.objects.all()
    serializer_class = ResidentSerializer


class BillViewSet(viewsets.ModelViewSet):
    queryset = Bill.objects.all()
    serializer_class = BillSerializer


class BillTypeViewSet(viewsets.ModelViewSet):
    queryset = BillType.objects.all()
    serializer_class = BillTypeSerializer


class AccountViewSet(viewsets.ModelViewSet):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer


def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            user = form.save()
            # Log the user in
            login(request, user)

            # Create a Resident object for the new user
            resident = Resident(user=user, name=user.username, surname="", address="", apartment_number=0)
            resident.save()

            # Redirect to the home page
            return redirect('home')
    else:
        form = RegisterForm()
    return render(request, 'register.html', {'form': form})


def user_login(request):
    if request.method == 'POST':
        form = LoginForm(data=request.POST)
        if form.is_valid():
            user = authenticate(username=form.cleaned_data['username'],
                                password=form.cleaned_data['password'])
            if user is not None:
                login(request, user)
                return redirect('home')
    else:
        form = LoginForm()
    return render(request, 'login.html', {'form': form})


@login_required  # Ensures that only logged-in users can access this view
def home(request):
    try:
        resident = Resident.objects.get(user=request.user)  # Get the resident associated with the logged-in user
        bills = Bill.objects.filter(resident=resident)  # Get all bills related to the resident
        account = Account.objects.get(resident=resident)

    except Resident.DoesNotExist:
        account = None
        resident = None
        bills = []

    context = {
        'resident': resident,
        'bills': bills,
        'account': account
    }
    
    return render(request, 'home.html', context, )


@login_required
def pay_bill(request, bill_id):
    # Get the bill object, ensuring that it belongs to the logged-in resident
    bill = get_object_or_404(Bill, id=bill_id, resident__user=request.user)
    
    if request.method == 'POST':
        # Mark the bill as paid and set the payment date
        bill.is_payed = True
        bill.payment_date = timezone.now()
        bill.save()
        
        # Redirect to a success page or back to the bills overview
        return redirect('home')
    else:
        # You can use a simple confirmation page here if needed
        return render(request, 'home.html', {'bill': bill})


@login_required
@require_POST
def top_up_account(request):
    try:
        resident = Resident.objects.get(user=request.user)
        account = Account.objects.get(resident=resident)
        amount = request.POST.get('amount')

        if amount and float(amount) > 0:
            account.balance += float(amount)
            account.save()
            messages.success(request, 'Счет успешно пополнен на сумму {}.'.format(amount))
        else:
            messages.error(request, 'Введите корректную сумму для пополнения.')

    except Resident.DoesNotExist:
        messages.error(request, 'Ошибка: не найден пользователь.')
    except Account.DoesNotExist:
        messages.error(request, 'Ошибка: не найден счет.')

    return redirect('home')