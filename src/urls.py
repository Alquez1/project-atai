from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import ResidentViewSet, BillViewSet, BillTypeViewSet, AccountViewSet, top_up_account, register, user_login, \
    home, pay_bill

router = DefaultRouter()
router.register(r'residents', ResidentViewSet)
router.register(r'bills', BillViewSet)
router.register(r'bill_type', BillTypeViewSet)
router.register(r'account', AccountViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('home/', home, name='home'),
    path('register/', register, name='register'),
    path('login/', user_login, name='login'),
    path('pay-bill/<int:bill_id>/', pay_bill, name='pay_bill'),
    path('top-up-account/', top_up_account, name='top_up_account'),
]
